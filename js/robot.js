Blockly.Blocks["obstaculo"] = {
    init: function() {
        this.jsonInit({
            "type": "obstaculo",
            "message0": "obstáculo",
            "args0": [],
            "output": "Boolean",
            "colour": 160,
            "tooltip": "Returns number of letters in the provided text.",
            "helpUrl": "http://www.w3schools.com/jsref/jsref_length_string.asp"
        });
    }
};
Blockly.Blocks["distancia"] = {
    init: function() {
        this.jsonInit({
            "type": "distancia",
            "message0": "distancia",
            "args0": [],
            "output": "Number",
            "colour": 160,
            "tooltip": "Returns number of letters in the provided text.",
            "helpUrl": "http://www.w3schools.com/jsref/jsref_length_string.asp"
        });
    }
};

Blockly.Blocks["luz"] = {
    init: function() {
        this.jsonInit({
            "type": "luz",
            "message0": "luz",
            "args0": [],
            "output": "Number",
            "colour": 160,
            "tooltip": "Returns number of letters in the provided text.",
            "helpUrl": "http://www.w3schools.com/jsref/jsref_length_string.asp"
        });
    }
};

Blockly.Blocks["avanzar"] = {
    init: function() {
        this.jsonInit({
            "type": "avanzar",
            "message0": "avanzar %1 %2",
            "args0": [
                {
                    "type": "input_value",
                    "name": "tiempo",
                    "check": "Number"
                },
                {
                    "type": "input_value",
                    "name": "velocidad",
                    "check": "Number"
                }
            ],
            "colour": 160,
            "tooltip": "Returns number of letters in the provided text.",
            "helpUrl": "http://www.w3schools.com/jsref/jsref_length_string.asp",
            "nextStatement": null,
            "previousStatement": null
        });
    }
};

Blockly.Blocks["retroceder"] = {
    init: function() {
        this.jsonInit({
            "type": "retroceder",
            "message0": "retroceder %1 %2",
            "args0": [
                {
                    "type": "input_value",
                    "name": "tiempo",
                    "check": "Number"
                },
                {
                    "type": "input_value",
                    "name": "velocidad",
                    "check": "Number"
                }
            ],
            "colour": 160,
            "tooltip": "Returns number of letters in the provided text.",
            "helpUrl": "http://www.w3schools.com/jsref/jsref_length_string.asp",
            "nextStatement": null,
            "previousStatement": null
        });
    }
};

Blockly.Blocks["girarIzquierda"] = {
    init: function() {
        this.jsonInit({
            "type": "girarIzquierda",
            "message0": "girarIzquierda %1 %2",
            "args0": [
                {
                    "type": "input_value",
                    "name": "tiempo",
                    "check": "Number"
                },
                {
                    "type": "input_value",
                    "name": "velocidad",
                    "check": "Number"
                }
            ],
            "colour": 160,
            "tooltip": "Returns number of letters in the provided text.",
            "helpUrl": "http://www.w3schools.com/jsref/jsref_length_string.asp",
            "nextStatement": null,
            "previousStatement": null
        });
    }
};

Blockly.Blocks["girarDerecha"] = {
    init: function() {
        this.jsonInit({
            "type": "girarDerecha",
            "message0": "girarDerecha %1 %2",
            "args0": [
                {
                    "type": "input_value",
                    "name": "tiempo",
                    "check": "Number"
                },
                {
                    "type": "input_value",
                    "name": "velocidad",
                    "check": "Number"
                }
            ],
            "colour": 160,
            "tooltip": "Returns number of letters in the provided text.",
            "helpUrl": "http://www.w3schools.com/jsref/jsref_length_string.asp",
            "nextStatement": null,
            "previousStatement": null
        });
    }
};
